# UseCase

## UseCase Diagram

```uml
@startuml
actor User as user
actor ProjectManager as pm

(設立專案) as createPJ
(設定/修改專案) as setPJ
(增加點餐時段) as newOrderTime
(設定/修改菜單) as setMenu
(點餐) as order

user <.. pm
pm -- createPJ
pm -- setPJ
pm -- newOrderTime
pm -- setMenu
setMenu <.. (設定公開的菜單) : extends
user -- (設定公開的菜單)
user -- order
@enduml
```



## Description

###使用者

使用者可以透過名稱尋找專案，並且透過輸入密碼進入專案點餐。

輸入密碼進入後，使用者可以根據管理員已經設定好的時段進行點餐，

例如，管理員設定「禮拜一午餐」、「年末尾牙聚餐」等等時段，
使用者點選後可以看到菜單，並且選擇需要的餐點（搜尋或新增）。

最後可以看到所選餐點的總金額。

為了因應可能產生的不同需求，使用者可以在特定時段輸入自己的需求，例如素食、不吃牛、去冰、無糖等等。

使用者若是長期特定飲食習慣者，可以在個人資料設定，常見設定有：素食、不吃牛、不吃豬等等。

###管理員

管理員可以設立專案，包含起迄時間、通關密碼、菜單。

管理員可以設定多個「時段」讓使用者點餐。當然，若菜單不同，可以選擇不同菜單。

管理員可以預先幫使用者輸入品名及價格，也可以讓使用者自行增加。

管理員可以更改使用者的狀態，

例如「未訂餐」、「未付款」、「已付款」等等。並且每次更改都會有記錄留存。

任何一個「時段」可以設定點餐截止時間，或是手動關閉、開啟點餐功能。

並且可以統計出該時段所有點餐的品名、數量、總價。

##Details

| 設立專案    |                                                              |
| ----------- | ------------------------------------------------------------ |
| Actor       | Project Manager (PM)                                         |
| Description | A PM is allow to create a project, including 'project name', 'password' for min. |
| CRUD        | CREATE Project with project name, password.                  |
| Simulate    | PM click 'create project'.                                   |
| Response    | A form that can be filled with.                              |
| Note        | Futher informations can be filled/revised after created one. |

| 設定/修改專案 |                                                              |
| ------------- | ------------------------------------------------------------ |
| Actor         | Project Managr (PM)                                          |
| Description   | A PM can revise or fill more information about the project.  |
| CRUD          | UPDATE certain project's 'begin/end date', upload 'menu', add new 'order time' |
| Simulate      | PM click 'manage the project'                                |
| Response      | All informations about the project, and able to revise ones. |
| Note          | Some may not be revisable.                                   |

| 設定/修改專案 -- 增加點餐時段 |                                                              |
| ----------------------------- | ------------------------------------------------------------ |
| Actor                         | Project Manager                                              |
| Description                   | A PM can create new 'order time' to a certain project,  chose menu, add some order. |
| CRUD                          | CREATE 'order time' to the project.                          |
| Simulate                      | click 'new order time'                                       |
| Response                      | A form...                                                    |
| Note                          |                                                              |

| 設定/修改專案 -- 設定菜單 |                                                              |
| ------------------------- | ------------------------------------------------------------ |
| Actor                     | Project Manager                                              |
| Description               | A PM can upload a menu and add some of its order and price corresponsd to it. Furthermore, if the PM chose the menu to be 'public', then anyone can add/revise item. However, to prevent from 'not good change', any change to the public menu must be comfirmed by the Maintainer. |
| CRUD                      | CREATE/UPDATE 'menu', with 'name', 'public/private'; 'item', 'price'. |
| Simulate                  | click upload menu or revise menu.                            |
| Response                  | A form with editible informations about the certain menu.    |
| Note                      |                                                              |

| 點餐        |                                                              |
| ----------- | ------------------------------------------------------------ |
| Actor       | User                                                         |
| Description | An user can click a certain 'order time' to order the male.  |
| CRUD        | CREATE 'male', with 'username', 'item', 'price'.             |
| Simulate    | click specific 'order time'.                                 |
| Response    | menu and items to choose from.                               |
| Note        | an user must enter a project by its password. (and thus become a member of the project.) |

| Name        |      |
| ----------- | ---- |
| Actor       |      |
| Description |      |
| CRUD        |      |
| Simulate    |      |
| Response    |      |
| Note        |      |

